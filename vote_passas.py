import colorama # couleur dans les résultats
from functools import cmp_to_key
import pandas as pd # intégration avec xlsx de sortie

categories = ["s'abstenir","n'incarnerait pas bien", "avis mitigé", "incarnerait bien", "incarnerait très bien", "incarnerait parfaitement"]
couleurs = [colorama.Back.WHITE,colorama.Back.RED,colorama.Back.LIGHTRED_EX, colorama.Back.YELLOW,colorama.Back.GREEN,colorama.Back.BLUE]

def genere_question(c : str,p : str) -> str:
    return "Je pense que {} _ le poste de {}".format(c,p)

def split(phrase :str) -> (str,str) : # renvoie le candidat et le poste, et l'index de la mention retenue
    # phrase = "Je pense que Intel _ le poste de Truc"
    if phrase[:13] != "Je pense que ":
        raise ValueError("Mauvaise phrase en entrée de split(phrase:str)->tuple")
    phrase = phrase[13:]

    # phrase = "Intel _ le poste de Truc"
    s = phrase.split(" ")
    
    c = s[0] # extrait le prénom
    phrase = phrase[len(c)+1:]
    
    # phrase = "_ le poste de Truc"
    s = phrase.split(" le poste de ")
    p = s[1]

    return c,p

# les mentions retenues sont celles qui conviennent à au moins la moité de la population
SEUIL_MEDIANE=0.50
SEUIL_ABSTENTION=0.50


# agrege les votes par catégorie
def agreg_votes(votes : list) -> list:
    l = [0] * len(categories)
    for v in votes:
        l[v] += 1
    return l


def print_votes(decision : str, votes_agreg : list,color = couleurs,ignore_abstention=True):
    TAILLE_BARRE=50 # taille du spectre des votes sur l'écran

    # afficher ou non l'abstention dans la barre de votes
    if ignore_abstention==False:
        scale = TAILLE_BARRE/sum(votes_agreg)
        l = len(votes_agreg)
    else:
        scale = TAILLE_BARRE/sum(votes_agreg[1:])
        l = len(votes_agreg)-1
    
    # dessin de la barre
    avanct_barre=0
    nb_caracteres = 0
    for i in range(l):
        i_ = len(votes_agreg)-i-1
        avanct_barre += scale*votes_agreg[i_]
        while(nb_caracteres < avanct_barre):
            nb_caracteres += 1
            if nb_caracteres==int(SEUIL_MEDIANE*(scale*sum(votes_agreg[1:])))+1:
                c="|"
            else:
                c=" "
            print(couleurs[i_]+c+colorama.Back.RESET,end="")

    # affiche le nom du vote, le taux d'abstention
    a = votes_agreg[0]/sum(votes_agreg)
    print(" -", decision, "- Abst =",a*100,"% - ", end = "")

    # affiche le jugement majoritaire, le pourcentage d'opposants (Minf) et de partisans (Msup)
    j,mi,ms = jugement_majoritaire_vote(votes_agreg)
    a = votes_agreg[0]/sum(votes_agreg)
    print("JugtMaj :", categories[j], "| Minf =",int(mi*1000)/10, "% - Msup =", int(ms*1000)/10, "% ",end="")
    
    print(votes_agreg, end="")

    if a > SEUIL_ABSTENTION:
        print(" - Hors course par abstention")
    else:
        print("")
    


# retourne le jugement majoritaire + les parts de votes pour des mentions sup et inf en cas de départage
def jugement_majoritaire_vote(votes_agreg : list) -> (int,list):
    mention_majoritaire = len(votes_agreg) # 1+meilleure mention
    total_votes_hors_abs = sum(votes_agreg[1:])
    a = 0 # votes pour les mentions parcourues
    while a/total_votes_hors_abs < SEUIL_MEDIANE:
        mention_majoritaire -= 1
        a += votes_agreg[mention_majoritaire]
    
    # calcul des nombres d'opposants et de partisans
    m_inf = sum(votes_agreg[1:mention_majoritaire])/total_votes_hors_abs # [1:] signifie qu'on exclut l'abstention
    m_sup = sum(votes_agreg[mention_majoritaire+1:])/total_votes_hors_abs
    return mention_majoritaire, m_inf, m_sup # pas de départage pour l'instant


# fonction de comparaison entre 2 candidats (nom,votes)
def sort_comp(c1,c2):
    m1,m_inf1,m_sup1 = jugement_majoritaire_vote(c1[1])
    m2,m_inf2,m_sup2 = jugement_majoritaire_vote(c2[1])

    a1 = c1[1][0]/sum(c1[1])
    a2 = c2[1][0]/sum(c2[1])

    if a1 > SEUIL_ABSTENTION and a2 < SEUIL_ABSTENTION:
        return +1
    elif a2 > SEUIL_ABSTENTION and a1 < SEUIL_ABSTENTION:
        return -1


    if m1 < m2:
        return +1
    if m1 > m2:
        return -1

    m = max([m_inf1, m_sup1, m_inf2, m_sup2])
    
    if m == m_sup1:
        if m >  m_sup2:
            return -1

    if m == m_sup2:
        if m >  m_sup1:
            return +1

    if m == m_inf1:
        if m >  m_inf2:
            return +1

    if m == m_inf2:
        if m >  m_inf1:
            return -1

    # le cas du départage très chiant qui reste sera fait à la main (cf wikipedia) si la situation l'impose
    print(" - ALED NEED DEPARTAGE ENTRE", c1[0], "ET", c2[0])

    # affiche le détail des votes et demande un résultat  
    print(c1[1],end=""), print_votes(c1[0],c1[1])
    print(c2[1],end=""), print_votes(c2[0],c2[1]) 
    i = int(input("Rentrer -1 si {} est mieux classé que {}, 1 sinon :".format(c1[0],c2[0])))
    assert i == -1 or i == 1
    
    return i


def mariage_stable(candidats : list, postes : list, preferences_candidats : dict, preferences_postes : dict):
    associations = {}
    candidats_libres = list(candidats)

    while candidats_libres:
        candidat = candidats_libres.pop(0)
        preferences_c = preferences_candidats[candidat]

        for poste in preferences_c:
            employe_actuel = associations.get(poste)
            if employe_actuel is None:
                # Le poste est libre, le candidat l'occupe
                associations[poste] = candidat
                break
            else:
                # Le poste est occupé, on compare les candidats
                preferences_p = preferences_postes[poste]
                if preferences_p.index(candidat) < preferences_p.index(employe_actuel):
                    # Le nouveau candidat est préféré
                    candidats_libres.append(employe_actuel)
                    associations[poste] = candidat
                    break

    return associations

# import des résultats des votes, et des voeux des candidat.e.s
from voeux import noms_postes, preference_candidats, file_vote


def genere_questions():
    for p in noms_postes:
        if "2" in p:
            continue
        l = sorted([ c for c in preference_candidats.keys() if p in preference_candidats[c] ])
        for c in l:
            print( genere_question(c,p) )


def main_resultats_vote():
    df = pd.read_excel(file_vote)

    # affichage des résultats
    for c in preference_candidats.keys():
        print(c, preference_candidats[c])

    assert input("Valider ? [y/n]") == "y"


    # postes = { (clé:str) : [ (candidat:str , votes_agreg:str) * n ] }
    postes = {}
    for p in noms_postes:
        postes[p] = []


    # itère sur les réponses de chaque question
    for nom_colonne, valeurs_colonne in df.items():
        
        if nom_colonne[0:4] == "Je p": # distingue les colonnes d'en-tête des colonnes de réponse aux questions
            candidat, poste = split(nom_colonne)
            
            # agrège les votes présents dans la colonne
            votes = []
            for v in valeurs_colonne.items():
                try:
                    votes.append(categories.index(v[1]))
                except:
                    votes.append(0) # s'abstenir en cas d'absence de réponse à la question
            votes_agreg = agreg_votes(votes)
            
            if poste == "VP Projets Vrac":
                postes[poste+" 1"].append((candidat,votes_agreg))
                postes[poste+" 2"].append((candidat,votes_agreg))
            else:
                postes[poste].append((candidat,votes_agreg))


    # classe les candidat.e.s sur chaque poste
    for p in postes.keys():
        print("Classement de {} en cours...".format(p))
        postes[p] = sorted(postes[p], key=cmp_to_key(sort_comp))
    
    for p in postes.keys():
        print("Classement pour {}".format(p))
        cl = 0 # (purement esthétique pour afficher le classement "1.", "2." ...)
        for e in postes[p]:
            cl += 1; print("{}. ".format(cl),end="")
            print_votes("{} en {}".format(e[0], p), e[1],ignore_abstention=False)


    # liste des candidats et des postes
    candidats_libres = list(preference_candidats.keys())
    postes_libres = list(noms_postes)

    # preferences_candidats a déjà été defini, il ne reste plus qu'à construire les classements de chaque poste
    preference_postes = {}
    for k in postes.keys():
        preference_postes[k] = [ i[0] for i in postes[k] ]


    # affectation avec l'algorithme des mariages stables
    resultat = mariage_stable(candidats_libres, postes_libres, preference_candidats, preference_postes)

    print(resultat)


def main_preparation():
    print(colorama.Style.BRIGHT + "-- Liste des questions :" + colorama.Style.RESET_ALL )
    genere_questions()
    print(colorama.Style.BRIGHT + "-- Liste des réponses possibles :" + colorama.Style.RESET_ALL)
    for m in categories: 
        print(m)


assert split(genere_question("Moi","Prez"))==("Moi","Prez")
assert len(categories) >= 5
assert agreg_votes([1,3,4,2,2,0,1,2,3]) == [1,2,3,2,1]+[0]*(len(categories)-5)

main_preparation()
main_resultats_vote()