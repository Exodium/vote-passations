noms_postes = [ "Prez", "Trez", "SecGen", "Vice-Prez", "VPrez Carbone", "VP Relations Campus", "VP Event", "VP Tri&Jardin", "VP Projets Vrac", "VP Com", "VPépin" ]

preference_candidats = {
    "Théo" : [ "Prez", "Trez" ], 
    "Capucine" : [ "Prez", "SecGen" ],
    "Milan" : [ "SecGen" ],
    "NG" : [ "SecGen", "Vice-Prez" ]
}


file_vote = "vote_tests.xlsx"