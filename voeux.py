noms_postes = [ "Prez", "Trez", "SecGen", "Vice-Prez", "VPrez Carbone", "VP Relations Campus", "VP Event", "VP Tri&Jardin", "VP Projets Vrac 1", "VP Projets Vrac 2", "VP Com", "VPépin" ]

preference_candidats = {
    "Alexandre" : [ noms_postes[0], noms_postes[5], noms_postes[1], noms_postes[3]],
    "Clément" : [ noms_postes[7] ],
    "Ulysse" : [ noms_postes[0], noms_postes[4], noms_postes[3], noms_postes[8], noms_postes[9]],
    "Mathieu" : [ noms_postes[5] ],
    "Guillaume" : [ noms_postes[-1], noms_postes[7], noms_postes[4]],
    "Malo" : [ noms_postes[1] ],
    "Yann" : [ noms_postes[2], noms_postes[-1], noms_postes[4], noms_postes[1]],
    "Lucie" : [ noms_postes[-1] ],
    "Tara" : [ noms_postes[8], noms_postes[9] ],
    "Raphaël" : [ noms_postes[0], noms_postes[3], noms_postes[4], noms_postes[5] ],
    "Juliette" : [ noms_postes[3], noms_postes[2], noms_postes[5], noms_postes[-2] ],
    "Anouchka" : [ noms_postes[0], noms_postes[6], noms_postes[1] ],
    "Luc" : [ noms_postes[3], noms_postes[-1], noms_postes[2] ],
    "Silvère" : [ noms_postes[2] ],
    "Pénélope" : [ noms_postes[6] ],
    "Emma" : [ noms_postes[1], noms_postes[3], noms_postes[6], noms_postes[4] ],
    "Titouan" : [ noms_postes[1], noms_postes[4], noms_postes[8], noms_postes[9] ],
    "Théo" : [ noms_postes[8], noms_postes[9] ]
}

file_vote = "votes_passas.xlsx"

print(sorted(preference_candidats.keys()))